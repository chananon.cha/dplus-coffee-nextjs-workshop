/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['www.nescafe.com', 'www.starbucks.co.th', 'upload.wikimedia.org'],
  },
}

module.exports = nextConfig
