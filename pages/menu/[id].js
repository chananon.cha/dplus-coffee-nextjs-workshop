/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import styles from '../../styles/Menu.module.css'
import Image from 'next/image'
import axios from 'axios'
import { FaStar, FaArrowLeft } from 'react-icons/fa'
import Link from 'next/link'


export async function getStaticPaths() {
    const config = {
        method: 'get',
        url: `https://cofffee-data.vercel.app/api/menu`
    }
    const res = (await axios(config)).data

    return {
        paths: res.map((el) => ({
            params: { id: el.id.toString() }
        })),
        fallback: false,
    }
}

export async function getStaticProps({ params }) {
    const config = {
        method: 'get',
        url: `https://cofffee-data.vercel.app/api/menu/${params.id}`
    }
    const res = (await axios(config)).data
    return {
        props: {
            singleMenu: res
        },
        revalidate: 30,
    }
}

const menuSinglePage = ({ singleMenu }) => {


    if (!singleMenu) return <h1>404 Not Found</h1>
    return (
        <div className={styles.container}>
            <div className={styles.back}>
                <FaArrowLeft />
                <Link href='/'>Back </Link>
            </div>
            <div className={styles.main}>
                <div className={styles.grid}>
                    <div className={styles.item}>
                        <div className={styles.image}>

                            <Image src={singleMenu.image} alt={singleMenu.name} layout='responsive' width={250} height={250} />
                        </div>
                    </div>
                    <div className={styles.item}>
                        <h3>#{singleMenu.id} {singleMenu.name}</h3>
                        {/* <h4>ส่วนประกอบ</h4> */}
                        <ul>
                            {
                                singleMenu.ingredient.map((el, i) => {
                                    return <li key={i} className={styles.tag}>{el}</li>
                                })
                            }
                        </ul>
                        <div className={styles.rating}>
                            {
                                Array(singleMenu.rating).fill(0).map((el, i) => {
                                    return <div key={i}>
                                        <FaStar />
                                    </div>
                                })
                            }
                        </div>


                    </div>
                </div>
            </div>
        </div>
    )
}

export default menuSinglePage